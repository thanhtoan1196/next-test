import React, { useState } from 'react';

function Search() {
    const [query, setQuery] = useState<string>('');
  return (
      <div>
        <input type="text" value={query} onChange={(e) => setQuery(e.target.value)}/>
        <div>value: {query}</div>
      </div>
  )
}

// Search.whyDidYouRender = true
export default Search
