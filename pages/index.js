import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Search from "../components/search";

export default function Home() {
  return (
    <div className={styles.container}>
      <Search/>
    </div>
  )
}
