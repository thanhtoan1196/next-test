import * as React from 'react';
import { GetStaticPaths, GetStaticProps } from 'next';
import { useRouter } from 'next/router';

export const getStaticProps: GetStaticProps = async (context) => {
    const slug = context.params?.slug;
    return {
        props: {
            slugParam: slug || ''
        },
        revalidate: 1
    };
};

const Posts: React.FC<{ slugParam: any }> = ({slugParam}) => {
    const router = useRouter();
    const { isFallback, query } = router;
    const { slug: slugQuery } = query;

    if (isFallback) {
        return null;
    }

    return (
        <div>
        <p>slugParam: {slugParam}</p>
        <p>slugQuery: {slugQuery}</p>
        </div>
    );
};

export const getStaticPaths: GetStaticPaths = async () => {
    return {
        paths: [],
        fallback: true
    };
};

export default Posts;
